
{-# LANGUAGE FlexibleContexts #-}
module Main where


import Language.Lex (mkPosToken,prToken,Token (..))
import Language.Par (myLexer,pTerm,pFunDef)
import Language.Print (Print,printTree)
-- import Data.Text (Text) 
import qualified Data.Text.IO as T
import System.Console.Haskeline
import Data.Text (Text)
import qualified Data.Text as Text (pack)
import Control.Monad.IO.Class (MonadIO)
-- import TypeInference
-- import TypeInferenceCorrect
import BoundTypeInference
-- import SimpleInterpreter
import TermUtils
import Data.Bifunctor (bimap)
import Control.Unification.IntVar
import Control.Applicative
import Control.Monad.Except
import Language.BoundAbs (BTerm (..))
import Control.Monad.State.Strict
import SimpleLangMonad
import SimpleInterpreter

import qualified Data.Map as M
import qualified Data.IntMap as IM

import BoundInterpreter
import Language.Abs (Term (..),FunDef(..))
import Language.AbsToBoundAbs
{-
To build the bnfc file, run build.sh
-}


showTree :: (Control.Monad.IO.Class.MonadIO m,Show a,Print a) => a -> InputT m ()
showTree tree = do
    outputStrLn $ "\n[Parse Tree]\n\n" ++ show tree
    outputStrLn $ "\n[Linearized tree]\n\n" ++ printTree tree

-- ((l,c),s) is line/row and column number from the token in string form
showPosToken :: (Show a,Show b,Show c) => ((a,b),c) -> String
showPosToken ((l,c),s) = concat [show l, ":" , show c, "\t", show s]


main :: IO ()
-- main = evalStateT (runInputT defaultSettings loop) (M.empty, M.empty) --,arbInitBinding)
main = driveLoop2

tryCompileStore ts = compileTerm ts <|> compileFunction ts

compileTerm :: [Token] -> SimpleLangMonadT String  (InputT (StateT (M.Map Text (Scheme IntVar),M.Map Text (BTerm Text)) IO)) ()
compileTerm ts = do
    term <- tryParseTerm ts
    (typeEnv,valEnv) <- lift $ lift get
    boundT <- liftBoundMaybe "Error: Ill defined pattern" $ absToBoundAbs term
    tyScheme <- tryTypeScheme (fullTypeInferError  typeEnv boundT)
    lift $ outputStrLn $ "\n[Type] \n    " ++ prettyPrintTypeFull tyScheme
    let boundEvald = weakHeadNormalizeWithEnv boundT valEnv
    lift $ outputStrLn $ "\n[Evaluation] \n    " ++ show boundEvald




compileFunction :: [Token] -> SimpleLangMonadT String  (InputT (StateT (M.Map Text (Scheme IntVar),M.Map Text (BTerm Text)) IO)) ()
-- compileFunction :: (MonadIO m, MonadState (a, b) m) => [Token] -> ExceptT String (InputT m) ()
compileFunction ts = do 
    funDef <- tryParseFunction ts
    lift $ outputStrLn $ "[Linearized Tree]\n    " ++ printTree funDef
    typeEnvRaw <- lift $ lift $ gets fst
    let typeEnv = M.intersection typeEnvRaw (freeVarsFunction funDef)
    boundF <- liftBoundMaybe "Error: Ill defined pattern" $  lFunToBoundFunction funDef -- change lFunToBoundFunction to give back a string with pattern location
    tyScheme <- tryTypeScheme (fullSchemeInferFunctionError typeEnv boundF) 
    lift $ outputStrLn $ "\n[Type]\n    " ++ prettyPrintSchemeIntVar tyScheme
    let (name,body) = getBFunNameBody boundF 
    lift $ lift $ modify $ bimap (M.insert name tyScheme) (M.insert name body)

tryTypeScheme tySchemeEither = case tySchemeEither of 
    Left msg -> do 
        lift $ outputStrLn $ "[Type error:]\n" ++ "    " ++ show msg
        throwError $ "[Type error:]\n" ++ "    " ++ show msg
    Right tyScheme -> return tyScheme
        

tryParseTerm :: (Monad m) => [Token] -> SimpleLangMonadT String m Term
tryParseTerm ts = case pTerm ts of
    Left err -> throwError $ "\nParse failed...\n" ++ "[Tokens]\n" ++ concatMap ( (++ "\n") . showPosToken . mkPosToken) ts ++ err ++ "\n"
    Right termTree -> return termTree

tryParseFunction :: (Monad m) => [Token] -> SimpleLangMonadT String m FunDef
tryParseFunction ts = case pFunDef ts of
    Left err -> throwError $ "\nParse failed...\n" ++ "[Tokens]\n" ++ concatMap ( (++ "\n") . showPosToken . mkPosToken) ts ++ err ++ "\n"
    Right termTree -> return termTree
    
driveLoop2 = do 
    res <- evalStateT (runInputT defaultSettings $ runExceptT  loop2) (M.empty, M.empty)
    case res of 
      Left s -> driveLoop2
      Right x0 -> putStrLn "finished"

        


loop2 = do 
    lift $ outputStrLn "\nNext term:"
    minput <- lift $ getInputLine "> "
    case minput of 
        Nothing -> loop2
        Just ":q" -> lift $ return ()
        Just "quit" -> lift $ return ()
        Just "exit" -> lift $ return ()
        Just ":m" -> do -- used for simple multiline input, new lines discarded
            ls <- getAllLines 
            let lsConc = concat ls
            tryCompileStore (myLexer  (Text.pack lsConc))
            loop2
        Just ":M" -> do -- used for structural multiline input, new lines kept
            ls <- getAllLines 
            let lsConc = unlines ls -- unlines inserts a \n at the end of every string in a list of strings
            tryCompileStore $ myLexer $ Text.pack lsConc
            loop2
        Just termString -> do 
            tryCompileStore (myLexer (Text.pack termString))
            loop2

getAllLines = do
    minput <- lift $ getInputLine ":> "
    case minput of 
        Nothing -> return []
        Just ":end" -> return []
        Just l -> do 
            ls <- getAllLines 
            return (l:ls)