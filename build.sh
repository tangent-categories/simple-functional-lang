#!/bin/bash

# Install mtl, alex, happy, unification-fd, bound.
stack install mtl
stack install unification-fd
stack install haskeline
stack install bound
stack install deriving-compat

# This will fail, but should pull in the rest of the dependencies
stack build

# check if the bnfc program is up to date 
BNFC_VERSION="$(bnfc --version)"
echo $BNFC_VERSION
echo $BNFC_VERSION
BNFC_REQUIRED="2.9.3"
echo $BNFC_REQUIRED
# The following checks if the bnfc is the correct version
if [ "$(printf '%s\n' "$BNFC_REQUIRED" "$BNFC_VERSION" | sort -V | head -n1)" = "$BNFC_REQUIRED" ]; then 
    echo "Recent enough BNFC version found"
else 
    echo "BNFC version too old, building newer version"
    cd ..
    git clone https://github.com/BNFC/bnfc.git
    cd bnfc/source 
    stack install --stack-yaml stack-9.0.1.yaml
   
fi

echo "we here"
# Rebuild the cf file with the correct options and then build the project
cd src
bnfc --haskell -d --text-token --functor Language.cf
# go into the Language folder
cd Language
# TODO: do a similar check to see if happy is installed
happy --info=grammar-ambigs.txt --outfile=grammar.out Par.y
# remove the grammar.out file; it's not needed
rm grammar.out
# leave the directory
cd ..
# we're now back in the src directory
# let's move the grammar-ambigs to the project root 
mv Language/grammar-ambigs.txt ../grammar-ambigs.txt

# Remove the unused file that also breaks the build
rm Language/Test.hs

# Insert similar stuff for building llvm, grin, z3, etc -- i.e. non-haskell tools needed.  Eventually 
# figure out how to build them without registering and pass the arguments in as needed

# Go to the prjoect root and build the project
cd ..
stack build

echo "build complete"
