module BoundInterpreter where

import Language.BoundAbs
import qualified Data.Bifunctor as Bifun

import Control.Monad.Reader

import qualified Data.Map as M
import Data.Text (Text)
import Bound

import Paternalize (paternalize)

{-
Okay, so whnf is supposed to be dead simple.
Let's see.
-}
weakHeadStep :: (Ord a) => BTerm a ->  Reader (M.Map a (BTerm a)) (BTerm a,Bool)
weakHeadStep t = case t of
  BV a -> do --return (BV a,False)
    varVal <- asks $ M.lookup a
    case varVal of
      Nothing -> return (BV a,False)
      Just bt -> return (bt,True)
  -- (\x.m) n = m[n/x]
  BApp (BLam sh m) n -> return (BLet (paternalize sh n) m,True)   
  BApp m n -> do --return $ Bifun.first (`BApp` n) (weakHeadStep m) -- otherwise by head normalization, we only look for a redex in the head position
    (m',changed) <- weakHeadStep m
    return (BApp m' n,changed)
  BLam sh sc -> return (BLam sh sc,False) -- weak normalization doesn't peak under lambdas
  BConst d -> return (BConst d, False)
  BUnit -> return (BUnit , False)
  BPair m n -> do
      (m',changed) <- weakHeadStep m
      if changed
        then return (BPair m' n,True)
        else do
          n' <- weakHeadStep n
          return $ Bifun.first (BPair m) n'
           --(weakHeadStep n) >>= (return . Bifun.first (BPair m)) -- first :: a -> b -> p a c -> p b c where in our case p is (,)
  BFst m -> do
    (m',changed) <-weakHeadStep m
    case m' of 
      (BPair ml mr) -> return (ml,True)
      _ -> return (BFst m',changed)
  BSnd m -> do
    (m',changed) <- weakHeadStep m
    case m' of
      (BPair ml mr) -> return (mr,True)
      _ -> return (BSnd m',changed)
  BLet ms n -> case ms of
    [] -> return (instantiate (const BUnit) n ,True) -- we only create BLet () n in compilation when we see Let () m n in Abs which is only created from let () = m in n in source
    _ -> return (instantiate (ms !!) n,True)


{-
The point is we've got weak head normalization as tail recursive.
-}
weakHeadNormalize :: (Ord a) => BTerm a ->  Reader (M.Map a (BTerm a)) (BTerm a)
weakHeadNormalize t = do
  (t',changed) <- weakHeadStep t
  if changed then weakHeadNormalize t' else return t

weakHeadNormalizeWithEnv :: (Ord a) => BTerm a -> M.Map a (BTerm a) -> BTerm a
weakHeadNormalizeWithEnv t = runReader (weakHeadNormalize t)


