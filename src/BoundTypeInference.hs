{-# Language DeriveTraversable #-}
{-# LANGUAGE FlexibleContexts  #-}

module BoundTypeInference where


import Control.Unification
import Control.Unification.IntVar
import Control.Unification.Types
import Language.Shape
import Control.Monad.Identity
import Control.Monad.Trans
import Control.Monad.Except
-- import Control.Monad.Reader
-- import Control.Monad.State.Strict
import Language.Abs (Pat' (..),Pat(..))
import qualified Data.List
import qualified Data.Map as M
import TermUtils
import Data.Text (Text)
import qualified Data.Text as Text (pack)

import Bound
import qualified Bound.Term as BT

import HasProj (HasLeftProj(..),HasRightProj(..))
import Paternalize (paternalize)

import Language.BoundAbs

{-# ANN module "HLint: ignore Use void" #-}

data TypeF a =
    R
    | TUnit
    | TPair a a
    | TArr a a
    deriving (Show,Eq,Functor,Foldable,Traversable)


  {-
  LAMBDA comment
  When performing the type inference for \lambda expressions, algorithm W
  inserts the variables with fresh types into the environment unbound.  This 
  forces a variable to be used by the exact same type across the entire term.
  See comment for Let bindings for the contrast.

  To type Lam _ pas te : a1 -> ... an -> b in Gamma 
  where the type of each pattern in pas unifies with ai
  we remove all all references to variables in pas from Gamma and add the fresh type variables 
  to Gamma, and then infer that m : b.   We then reset gamma to what it was.
  -}
  {-
  LET comment
  When performing the type inference for Let bindings, algorithm W 
  insert the variables with *bound* types into the environment.  This allows 
  different uses of let bound variables to be used differently in a term.
  This also makes let x = m in n different from (\lambda x . m) n, as the former 
  latter, by the typing for Lambda expressions the type of x is unbound and thus 
  required to be a single (generic) type.

  To illustrate the difference consider that 
      \lambda f. (f 1, f (1,1))
  Does not type.  However 
      let f = \x . x in (f 1, f(1,1))
  Does.  
  Now if we were to try and "expand" the let expression by reversing
      (\lambda x . m) n ~~> let x = n in m
  We would write 
      (\f. (f1, f(1,1)) )  \x.x
  Which we know fails to type.      

  Let p = m in n.
  So first, we clone the environment, and infer the type of m *outside of the monad.*  Then we fully abstract m's type, and store 
  the appropriate reference in the environment.
  -}


pair m n = UTerm $ TPair m n
unit = UTerm TUnit
real = UTerm R
arr m n = UTerm $ TArr m n

{-
Neat right.  This is all you have to write to get unification.
-}
instance Unifiable TypeF where
    zipMatch R R = Just R
    zipMatch TUnit TUnit = Just TUnit
    zipMatch (TPair l1 r1) (TPair l2 r2) = Just $ TPair (Right (l1,l2)) (Right (r1,r2))
    zipMatch (TArr l1 r1) (TArr l2 r2) = Just $ TArr (Right (l1,l2)) (Right (r1,r2))
    zipMatch _ _ = Nothing

-- some convenient types 
type Type = UTerm TypeF IntVar
type IntBinding = IntBindingT TypeF Identity
type TypingFailure = UFailure TypeF IntVar

-- Convenience function for getting a fresh variable in the binding monad
getFreshVar :: (MonadTrans t1,BindingMonad t2 v m) => t1 m (UTerm t2 v)
getFreshVar = lift $ UVar <$> freeVar

{-
Warning!  If you try to generlize the following to return an infinite stream of type 
variables and then "take" them as needed you will get a runtime error.  As the saying 
goes, there's lazy and then there's **lazy**  Haskell is merely lazy.  Indeed, the various 
monads involved will force, through the use of bind, full evaluation of [1..] and the 
computation will diverge.
-}
streamFreshVars :: (MonadTrans t1,BindingMonad t2 v m,Monad (t1 m)) => Int -> t1 m [UTerm t2 v]
streamFreshVars n = mapM (const getFreshVar) [1 ..n]

--  ExceptT TypingFailure (IntBindingT TypeF (ExceptT String (StateT (TypeEnv IntVar) m))) Type

{-
A typing context is a function that maps variables in a term to type schemes.
First recall that a scheme is a scheme for the polymorphism in a type: Forall a1,...,an. type[a1,...,an]
This makes the Forall quantifier into a type variable binding.  In Haskell, we represent this using
the binding syntax.
-}
data Scheme a = Forall Int (Scope Int (UTerm TypeF) a)

{-
Often we represent typing rules as derivations on terms in context.  However we can view them equally well as 
derivations on a kind of type closure -- that is -- the closing of its free variables and storage of the 
types that are captured in the closure (thinking of a type as a kind of value).  
   \Gamma \proves m : B 
This actually models the sense in which we take terms in context \Gamma \proves m : B to be up to alpha-renaming 
of the variables in \Gamma.  In fact, one can imagine re-arranging 
hypothetical judgments to showcase the binding as \proves (x1,...,xn; m ; A1,...,An) : B.  (where \Gamma = (xi:Ai)_i).

Then any instantiation of the term is free to rename the variables, but the types they are assigned must be preserved.
\
We abstract this into the following data type (where we view a context as a pattern, hence a Scope Int) using the Bound library.
  Scope Int BTerm a: denotes a binding of Ints to variables in the BTerm.
  [a]: denotes the types of the variables in the term.  Usually, you will want to think of 'a' as Scheme b or realy Scheme IntVar.
  # Note that when calling full instantiate, it's important that the Scope was formed with respect to the order in the type scheme.
-}
data TClosure a = Proves (Scope Int BTerm a) [a]

{-
To supply the unification required for type inference, we use the unification binding monad provided by Unification-fd.
One may notice that we thus no longer require a state-monad.  Before we delve into the 
type inference algorithm, we set up a few convenience functions for instantiating and abstracting types.
-}



{-
This function takes a type and fully abstracts all of its free type variables creating a closed scheme.
We assume this is performed outside the monad (that is the type being stored had all bindings resolved),
and so we use our own free variable collector because we can't use the bindings in the monad.
-}
fullyAbstractType :: (Eq a) => UTerm TypeF a -> Scheme a
fullyAbstractType ty =
    let (size,theFreeVars) = freeVarsTypeList ty
    {-
    By construction length theFreeVars == size.  Hence if size == 0, then listToMap theFreeVars is (extensionally) equal to const Nothing.
    Then in fullInstantiateWithFresh, we will see the case EQ.  Then any time listToMap theFreeVars x returns Just _, x will be instantiated 
    with ().  However, listToMap theFreeVars is const Nothing, so the case of EQ will project "ty" as given here.
    In other words fullInstantiationWithFresh (fullyAbstractType ty) == ty whenever size == 0.
    -}
    in Forall size $ abstract (listToMap theFreeVars) ty

{-
Similarly, given a BTerm b, we want to fully abstract out its variables into a TClosure.  
However, recall that a TClosure requires us to keep the type scheme for reinstantiation.
Thus we need a way to convert the term to a BTerm (Scheme a).  However, as BTerm a is traversable, 
Scope Int BTerm is traversable.  Thus we can use the function closed from Bound.Term to 
freely convert the type.  It fails if the term is not closed.  Thus we 
  1. Fully abstract the term to a (Scope Int BTerm b)
  2. Apply closed to obtain a Maybe (Scope Int BTerm (Scheme a))
And we return the result of 2.  Importantly, we don't need to rely on any globally-unique-supply of variable 
names.

Note: this function will fail with "Symbol not in scope" if a variable in the term is not in the Map of freeVariables to 
  typeSchemes.
-}
closeTermType :: (Eq b,Ord b)
  => BTerm b                                  -- the term being closed
  -> M.Map b a                                -- the mapping of free variable names to type schemes
  -> Either String (TClosure a)               -- the resulting closed term
closeTermType t env =
  let (theFreeVars,typeSchemes) = unzip $ M.toList env in  -- freeVars :: [b]  and typeSchemes :: [a] and length freeVars == length typeSchemes so that freeVars[i] : typeSchemes[i] in env.
  let abstractedTerm = abstract (listToMap theFreeVars) t in -- abstractedTerm :: Scope Int BTerm b
  case BT.closed abstractedTerm of
    Nothing -> Left "Error: symbol out of scope" -- add a search and find for the offender
    Just t_butClosed -> Right $ Proves t_butClosed typeSchemes


{-
This function re-instantiates a type scheme with brand new fresh variables.
So this version happens in the monad.  There is also a pretty printer 
version of this which instantiates type variables with a default stream of letters 
to allow a more canonical looking type.
-}
fullInstantiateWithFresh :: (MonadTrans t1, BindingMonad TypeF a m, Monad (t1 m))
    => Scheme a ->  t1 m (Maybe (UTerm TypeF a))
fullInstantiateWithFresh (Forall n sc) = case compare 0 n of
    EQ -> return $ Just $ instantiate (const (UTerm TUnit)) sc -- it's a type with no bound variables, so we just project the type out
    LT -> do -- this is the case that we have Forall a0,...,a_{n-1}. ty
        streamedFresh <- streamFreshVars n
        let freshiesNeeded = take n streamedFresh
        return $ Just $ instantiate (freshiesNeeded !!) sc
    GT -> return Nothing -- if n < 0 , that would mean the scheme has negatively many bound variables...

{-
We don't need a separate way to instantiate a closure.  A type closure contains the information required to instantiate it.
We then get a term where the leaves have the typeScheme in them already.  All then that we have to do is visit them, and 
fullInstantiateWithFresh.
-}

{-
This function should remind you why you love functional programming.
elemIndex :: Eq a => a -> [a] -> Maybe Int 
which "searches" for an element in a list may be equally regarded by flipping the arguments around 
as 
"elemIndex" :: Eq a => [a] -> a -> Maybe Int
which takes in a list and gives you a characteristic function for the list.
-}
listToMap :: Eq a => [a] -> a -> Maybe Int
listToMap = flip Data.List.elemIndex

{-
Collect the free variables of a type from left to right
 -}


freeVarsTypeList :: UTerm TypeF a -> (Int,[a])
freeVarsTypeList ty = freeVarsTypeListAccum ty 0 []

freeVarsTypeListAccum ty sizeAcc varAcc = case ty of
    UVar a -> (sizeAcc+1,a:varAcc)
    UTerm R -> (sizeAcc,varAcc)
    UTerm TUnit -> (sizeAcc,varAcc)
    UTerm (TPair a b) ->
        let (newSize,newAcc) = freeVarsTypeListAccum b sizeAcc varAcc
        in freeVarsTypeListAccum a newSize newAcc
    UTerm (TArr a b) ->
        let (newSize,newAcc) = freeVarsTypeListAccum b sizeAcc varAcc
        in freeVarsTypeListAccum a newSize newAcc



inferClosure :: (Monad m)
  => TClosure (Scheme IntVar) -- ^ The closure being typed  -- typically, concretely, you want to think of 'a' as being Scheme IntVar
  -> Type -- The expected type
  -> ExceptT TypingFailure (IntBindingT TypeF (ExceptT String m)) (UTerm TypeF IntVar) -- the resulting type of the closure threaded through some monad transformers
inferClosure (Proves cont envTys)  = inferTerm (instantiate (BV . (envTys !!)) cont)

{-
Now we describe type the type inference algorithm as an extended algorithm W following Damas-Milner.
-}
inferTerm :: (Monad m)
  => BTerm (Scheme IntVar)
  -> Type
  -> ExceptT TypingFailure (IntBindingT TypeF (ExceptT String m)) (UTerm TypeF IntVar)
inferTerm term ty = case term of
  BV tySc -> do
    tyFreshed <- fullInstantiateWithFresh tySc
    case tyFreshed of
      Nothing -> lift $ lift $ throwError "Symbol not in scope" -- add pass through to figure out which symbol -- needs to be threaded through.
      Just tyActual -> do
        tyActual =:= ty
  BApp m n -> do
      a_var <- getFreshVar
      b_var <- getFreshVar
      x_var <- getFreshVar
      x <- inferTerm m x_var -- m : X
      a <- inferTerm n a_var -- n : A
      x =:= arr a b_var      -- X = A -> B
      ty =:= b_var           -- m n : B
  BConst x -> do
      ty =:= real
  BUnit -> do
      ty =:= unit
  BPair m n -> do
      a_var <- getFreshVar
      b_var <- getFreshVar
      a <- inferTerm m a_var
      b <- inferTerm n b_var
      ty =:= pair a b
  BFst m -> do
      a_var <- getFreshVar
      b_var <- getFreshVar
      x_var <- getFreshVar
      x <- inferTerm m x_var
      x =:= pair a_var b_var
      ty =:= a_var
  BSnd m -> do
      a_var <- getFreshVar
      b_var <- getFreshVar
      x_var <- getFreshVar
      x <- inferTerm m x_var
      x =:= pair a_var b_var
      ty =:= b_var
  {-
  sh is the shape of the pattern in the lambda expression
  sc is the scope creating the locally nameless term of the body of the lambda term
  We refer to this node as \lambda p . m 
  -}
  BLam sh sc -> do
    (pTy,pTyVarList) <- shapeToTypeAndTyVarList sh  -- pTy :: Type   pTyVarList :: [Type].  pTy is the type of pattern p.  pTyVarList is the types of each variable in the pattern, left to right. sizTyList :: Int is the number of type variables
    -- the schemes produced by shapeToTypeAndTyVarList are all free (no bindings at all).  Since they are free and we want the type variables bound by lambda to be free (see above), we need only to instantiate
    let m = instantiate (BV . (pTyVarList !!)) sc
    -- and now we prove that m : B
    bVar <- getFreshVar
    b <- inferTerm m bVar
    ty =:= arr pTy b -- wow this is so much cleaner than in TypeInferenceCorrect.hs and it's much closer to the spec.    
  BLet ms sc -> do
    -- recall that in converting terms to bterms we already took care of the binding of the terms ms into the term n.
    -- thus all we need to do is convert all the terms in ms into type schemes and then instantiate.  
    -- However, recall that we have to make fully abstracted schemes out of the resulting types, so we need to leave the 
    -- monad and perform full scheme inference on the terms.  To do that, we have to close the terms.  However, due to the 
    -- type of ms, as [BTerm (Scheme IntVar)], we can't just apply the closeTermType function because Scheme IntVar isn't Ord.
    -- So what we do is convert the ms to a [BTerm IntVar], by 
    msTypes <- lift $ lift $ liftEither $ mapM fullyInferTermError ms  -- msTypeSchemes :: [Type] so we just need to fully abstract out all the types
    let msTypeSchemes = map fullyAbstractType msTypes
    -- now we instantiate the scope with the types 
    let n = instantiate (BV . (msTypeSchemes !!)) sc
    -- and now we type that n : B
    b_var <- getFreshVar
    b <- inferTerm n b_var
    ty =:= b 

{-
Oh my this is even cleaner than TypeInferenceCorrect.hs
-}

fullyInferTermError m = case fullyInferTerm m of
  Left msg -> Left msg
  Right t -> case t of
    (Left unifError) -> Left $ show unifError
    (Right ty) -> Right ty

fullyInferTerm m = unwrapTypeMT fullyInferredTerm
  where
    fullyInferredTerm = do
      q <- getFreshVar
      inferTerm m q
      applyBindings q

{-
Now we bake the prolog query directly in.  Given m, does there exists a q such that m : q.
-}
inferQuery m = do
  q <- getFreshVar
  inferTerm m q

inferQueryClosure m = do
  q <- getFreshVar
  inferClosure m q

{-
  The job's not done until we apply the bindings out.
-}
inferAndApply m = do
  theTy <- inferQuery m
  applyBindings theTy

inferAndApplyClosure m = do
  theTy <- inferQueryClosure m
  applyBindings theTy

{-
This is a convenience function to peel back some of the monad transformers.
This assumes the identity monad was used.  If we move to the logicT monad 
to perform backtracking replace runIdentity with runLogic
-}
unwrapTypeMT :: ExceptT e1 (IntBindingT t (ExceptT e2 Identity)) a -> Either e2 (Either e1 a)
unwrapTypeMT = runIdentity . runExceptT . evalIntBindingT . runExceptT

{-
Now we perform full type inference on a term in context.  To do this we close the term in the context and 
perform inference on the closure.
-}
fullTypeInfer cont term = unwrapTypeMT inferredType
  where
    inferredType = do
      termClosed <- lift $ lift $ liftEither $ closeTermType term cont
      inferAndApplyClosure termClosed



fullTypeInferError cont term = case fullTypeInfer cont term of
  Left msg -> Left msg
  Right (Left unifError) -> Left (show unifError)
  Right (Right ty) -> Right ty

fullSchemeInferError cont term = case fullTypeInferError cont term of
  Left msg -> Left msg
  Right ty -> Right $ fullyAbstractType ty


fullSchemeInferFunctionError cont (BFunction _  body) = fullSchemeInferError cont body

{-
NOTE: Only for use in type inference of lambda expressions (or other local binders.  let and similar terms are inherently polymorphic and this shouldn't be used there.
This creates a type from a shape and a list of *free* type variables associated with the pattern variables of the shape.
-}
shapeToTypeAndTyVarList pat = case pat of
  UShape -> do
    q <- getFreshVar -- this time we want only the variable, but due to some weirdness of type classes getFreshVar wants to return a UTerm TypeF a thing, but makes it into a UTerm TypeF (UTerm TypeF a) thing...  This makes me a little nervous, ngl.
    case q of
      UVar qVar -> do
        q =:= UTerm TUnit
        return (q,[Forall 0 (abstract (const Nothing) (UTerm TUnit))])
      _ -> lift $ lift $ throwError "Bug!  getFreshVar produced a non-variable term"
  Done -> do
    q <- getFreshVar
    case q of
      UVar qVar -> do
        return (q,[Forall 0 (abstract (const Nothing) (UVar qVar))]) -- var
      _ -> lift $ lift $ throwError "Bug! getFreshVar produced a non-variable term"
  PairS p q -> do
    (a,varsL) <- shapeToTypeAndTyVarList p
    (b,varsR) <- shapeToTypeAndTyVarList q
    pq <- getFreshVar
    pq =:= pair a b
    return (pq,varsL ++ varsR)


-- pretty printing section

-- We assume an ample supply of variable names is given.
fullInstantiateWith :: Scheme a -> [a] -> UTerm TypeF a
fullInstantiateWith (Forall n scheme) vars =
    let neededVars = take n vars in -- :: [a]
    let absFun = \n -> UVar (neededVars !! n) in -- Int -> Identity a
    let theTy = instantiate absFun scheme in
    theTy

{-
This is just a default supply of string names to make a type look prettier and canonical.  In particular it's the stream 
  a,b,...,z,a0,b0...,a1,b1,...,a2,b2,....,
-}
defaultStringVarSupply :: [String]
defaultStringVarSupply =
    stringAtoZ ++
    [a ++ show n | n <- [0 ..] , a <- stringAtoZ]


stringAtoZ :: [String]
stringAtoZ = map return ['a' .. 'z']

defaultTextVarSupply :: [Text]
defaultTextVarSupply = map Text.pack defaultStringVarSupply

defaultIntSupply :: [Int]
defaultIntSupply = [0 ..]

defaultIntVarSupply :: [IntVar]
defaultIntVarSupply = map IntVar defaultIntSupply

-- To prettify a type, fully abstract it and then fully instantiate with the default string var supply
prettifyTypeVars :: Show a => UTerm TypeF a -> UTerm TypeF String
prettifyTypeVars ty = fullInstantiateWith (fullyAbstractType ( fmap show ty )) defaultStringVarSupply

prettyPrintType :: UTerm TypeF String -> String
prettyPrintType ty = case ty of
    UVar s -> s
    UTerm tf -> case tf of
      R -> "R"
      TUnit -> "(" ++ "_"
      (TPair ut ut') -> "(" ++ prettyPrintType ut ++ "," ++ prettyPrintType ut'  ++ ")"
      (TArr ut ut') -> case ut of
          t@(UTerm (TArr a b)) -> "(" ++  prettyPrintType t ++ ")" ++ " " ++ "->" ++ " "  ++ prettyPrintType ut'
          _ -> prettyPrintType ut  ++ " " ++ "->"  ++ " " ++ prettyPrintType ut'

prettyPrintTypeFull :: Show a => UTerm TypeF a -> String
prettyPrintTypeFull = prettyPrintType . prettifyTypeVars

-- projType = UTerm (TArr (UTerm (TPair (UVar (IntVar 0)) (UVar (IntVar 1)))) (UVar (IntVar 0)))

prettifySchemeStringWith :: Show a => Scheme a -> [a] -> String
prettifySchemeStringWith scheme vars = prettyPrintTypeFull $ fullInstantiateWith scheme vars

prettyPrintSchemeText :: Scheme Text -> String
prettyPrintSchemeText scheme = prettifySchemeStringWith scheme defaultTextVarSupply
prettyPrintSchemeString :: Scheme String -> String
prettyPrintSchemeString scheme = prettifySchemeStringWith scheme defaultStringVarSupply
prettyPrintSchemeInt :: Scheme Int -> String
prettyPrintSchemeInt scheme = prettifySchemeStringWith scheme defaultIntSupply
prettyPrintSchemeIntVar :: Scheme IntVar -> String
prettyPrintSchemeIntVar scheme = prettifySchemeStringWith scheme defaultIntVarSupply


-- testing section
-- The smart constructors defined here are for testing only
-- Testing TODO: Make into proper tests with the haskell testing frameworks

bvar = BV
bvt = BV . Text.pack
bletVar xsms n = let (xs,ms) = unzip xsms in BLet ms (abstract (listToMap xs) n)
blett xsms n = let (xs,ms) = unzip xsms in BLet ms (abstract (listToMap (map Text.pack xs)) n)
bconst = BConst
bpair = BPair
blamVar x m = BLam Done (abstract (listToMap [x]) m)
blamUnit x m = BLam UShape $ abstract (const Nothing) m
blamPat pat m = let (patShape,patVars) = patToShTyVarList pat in BLam patShape $ abstract (listToMap patVars) m
bapp = BApp
blamT x = blamVar (Text.pack x)




bupat = UShape
bspat = Done
bpairpat = PairS

patToShTyVarList p = case p of
  UnitPattern _ -> (UShape,[])
  Singleton _ x -> (Done,[pIdentToText  x])
  PairPattern _ p q -> let (sp,pvars) = patToShTyVarList p in let (sq,qvars) = patToShTyVarList q in (PairS sp sq,pvars ++ qvars)

  -- data Pat' a
  --   = UnitPattern a
  --   | Singleton a PIdent
  --   | PairPattern a (Pat' a) (Pat' a)

egTerm = bletVar [("x",bconst 3.3)] (bpair (bvar "x") (bvar "x")) -- : (R,R)


testEgTerm = case fullTypeInferError M.empty egTerm of
    (Right (UTerm (TPair (UTerm R) (UTerm R) ))) -> True
    _ -> False
egTerm2 = bpair (bvar "x") (bvar "x") -- fails in empty context. in context x:(R,R) egTerm2 : ((R,R),(R,R))
test1EgTerm2 = case fullTypeInferError M.empty egTerm2 of
    Left _ -> True
    _ -> False
test2EgTterm2 = case fullTypeInferError (M.map fullyAbstractType (M.insert "x" (pair real real) M.empty)) egTerm2 of
    (Right (UTerm (TPair (UTerm (TPair (UTerm R) (UTerm R))) (UTerm (TPair (UTerm R) (UTerm R))) )))
        -> True
    _ -> False
egTerm3 = bvar "x" -- fails in empty context.  in context x:(R,R) egTerm3 : (R,R)
test1EgTerm3 = case fullTypeInferError M.empty egTerm3 of
    Left _ -> True
    _ -> False
test2EgTerm3 = case fullTypeInferError (M.map fullyAbstractType (M.insert "x" (pair real real) M.empty)) egTerm3 of
    (Right (UTerm (TPair (UTerm R) (UTerm R) ))) -> True
    _ -> False

-- -- example that fails type inference due to a match failure
-- -- (\(x,y).x) 3.1
egTermMatchFailure =
    let xypat = pPat (sPat "x") (sPat "y")
    in let lamxyx = blamPat xypat (bvar (Text.pack "x"))
       in bapp lamxyx (bconst 3.1)


testEgTermMatchFailure = case fullTypeInfer M.empty egTermMatchFailure of
    (Right (Left (MismatchFailure _ _))) -> True
    _ -> False

fstTermSyn =
    let xypat = pPat (sPat "x") (sPat "y")
    in blamPat xypat (bvt "x")
sndTermSyn =
    let xypat = pPat (sPat "x") (sPat "y")
    in blamPat xypat (bvt "y")

fstT = bapp fstTermSyn -- fst(m)
sndT = bapp sndTermSyn -- snd(m)
-- -- example terms that succeed on projecting out of a product
-- let x = (1.1,(1.1,1.1)) in fst(x)
egTermProjPair = blett [("x",bpair (bconst 1.1) (bpair (bconst 1.1) (bconst 1.1)))] (fstT (bvt "x"))
testegTermProjPair = case fullTypeInferError M.empty egTermProjPair of
    Right (UTerm R) -> True
    _ -> False
-- -- the same term as egTermProjPair (to withing alpha equivalence) but with a fresh variable z
egTermProjPair2 = blett [("x", bpair (bconst 1.1) (bpair (bconst 1.1) (bconst 2.3)))] $ fstT (bvt"x")
testegTermProjPair2 = case fullTypeInferError M.empty egTermProjPair2 of
    Right (UTerm R) -> True
    _ -> False

egTermProjPair3 = blett [( "x", bpair (bconst 1.1) (bpair (bconst 1.1) (bconst 2.3)))] (sndT (bvt "x"))
testegTermProjPair3 = case fullTypeInferError M.empty egTermProjPair3 of
    Right (UTerm (TPair (UTerm R) (UTerm R))) -> True
    _ -> False
egTermProjPair4 = blett [( "x", bpair (bconst 1.1) (bpair (bconst 1.1) (bconst 2.3)))] (sndT (sndT (bvt "x")))
testegTermProjPair4 = case fullTypeInferError M.empty egTermProjPair4 of
    Right (UTerm R) -> True
    _ -> False

-- TODO: Make 5 and 6 tests
-- egTermProjPair5 = fstT (pairT (pairT (constT 1.0) (constT 1.0)) (constT 1.0))
-- egTermProjPair6 = fstT (pairT (constT 1.0) (pairT (constT 1.0) (constT 1.0)))

-- let x = 1.1 in let x = (0.1,0.5) in x
egTermShadowBind = blett [("x",bconst 1.1)] $ blett [("x",bpair (bconst 0.1) (bconst 0.5))] (bvt "x")
testEgTermShadowBind = case fullTypeInferError M.empty egTermShadowBind of
    Right (UTerm (TPair (UTerm R) (UTerm R))) -> True
    _ -> False

-- -- example for simply disregarding information
egTermDisregard = blett [( "x", bconst 1.1)] $ bpair (bconst 5) (bconst 7) -- : (R,R)
egTermDisregard2 = blett [("x", bpair (bconst 5) (bconst 7))] (bconst 1.9) -- : R

testEgTermDisregard = case fullTypeInferError M.empty egTermDisregard of
    Right (UTerm (TPair (UTerm R) (UTerm R))) -> True
    _ -> False
testEgTermDisregard2 = case fullTypeInferError M.empty egTermDisregard2 of
    Right (UTerm R) -> True
    _ -> False

testUnit = case fullTypeInferError M.empty (BUnit ::BTerm Text) of
    Right (UTerm TUnit) -> True
    _ -> False

egTermUnitDisregard = blett [( "x", BUnit)] (bconst 9)
egTermUnitDisregard2 = blett [("x", bconst 55)] BUnit

testEgTermUnitDisregard = case fullTypeInferError M.empty egTermUnitDisregard of
    Right (UTerm R) -> True
    _ -> False

testEgTermUnitDisregard2 = case fullTypeInferError M.empty egTermUnitDisregard2 of
    Right (UTerm TUnit) -> True
    _ -> False

-- -- \x y . x y 
egTermCodeEv = blamVar "x" $ blamVar "y" $ bapp (bvar "x") (bvar "y")
testEgTermCodeEv = case fullTypeInferError M.empty egTermCodeEv of
    -- Right (UTerm (TArr (UTerm (TArr (UTerm x) (UTerm y))) (UTerm (TArr (UTerm z) (UTerm w))) ) )
    Right (UTerm (TArr (UTerm (TArr (UVar x) (UVar y)) ) (UTerm (TArr (UVar z) (UVar w)))))
        -> x == z && y == w
    _ -> False

-- -- \(x,y). x y
egTermCodeEv2 = blamPat (pPat (sPat "x") (sPat "y")) $ bapp (bvt "x") (bvt "y")
testEgTermCodeEv2 = case fullTypeInferError M.empty egTermCodeEv2 of
    Right (UTerm (TArr ( UTerm (TPair ( UTerm (TArr (UVar x) (UVar y)) ) (UVar z)) ) (UVar w)))
        -> x == z && y == w
    _ -> False

-- -- (let x = 1 in (x,x),let y = (1,1) in y)
egLetBranchPair = bpair (blett [("x", bconst 1)] (bpair (bvt "x") (bvt "x"))) (blett [("y", bpair (bconst 1) (bconst 1))] (bvt"y"))
testEgLetBranchPair = case fullTypeInferError M.empty egLetBranchPair of
    (Right (UTerm (TPair (UTerm (TPair (UTerm R) (UTerm R))) (UTerm (TPair (UTerm R) (UTerm R))) )))
        -> True
    _ -> False

-- this one needs to fail as x doesn't exist on the right branch
egLetBranchPair2 = bpair (blett [("x", bconst 1)] (bpair (bvt "x") (bvt "x"))) (blett [("y", bpair (bconst 1) (bconst 1))] (bvt "x"))
-- we use fullTypeInfer to ensure that this is a variable scope issue
testEgLetBranchPair2 = case fullTypeInferError M.empty egLetBranchPair2 of
    Left _ -> True
    _ -> False

-- same as egLetBranchPair but reusing the variable name x -- which has different types on the different branches
egLetBranchPair3 = bpair (blett [("x", bconst 1)] (bpair (bvt "x") (bvt "x"))) (blett [("x", bpair (bconst 1) (bconst 1))] (bvt "x"))
testEgLetBranchPair3 = case fullTypeInferError M.empty egLetBranchPair3 of
    (Right (UTerm (TPair (UTerm (TPair (UTerm R) (UTerm R))) (UTerm (TPair (UTerm R) (UTerm R))) )))
        -> True
    _ -> False

-- here x has different types on the different branches and the branches themselves have different types
-- (let x = 1 in (x,x), let x = (1,1) in (x,x)) : ((R,R),((R,R),(R,R)))
egLetBranchPair4 = bpair (blett [("x", bconst 1)] (bpair (bvt "x") (bvt "x"))) (blett [("x", bpair (bconst 1) (bconst 1))] (bpair (bvt "x") (bvt "x")))
testEgLetBranchPair4 = case fullTypeInferError M.empty egLetBranchPair4 of
    (Right (UTerm (TPair (UTerm (TPair (UTerm R) (UTerm R))) (UTerm (TPair (UTerm (TPair (UTerm R) (UTerm R))) (UTerm (TPair (UTerm R) (UTerm R))) )))))
        -> True
    _ -> False


-- -- This example should fail the occurs check
egTermSelfApp = blamVar (Text.pack "x") (bapp (bvt "x") (bvt "x"))
testEgTermSelfApp = case fullTypeInfer M.empty egTermSelfApp of
    Right (Left (OccursFailure _ _)) -> True
    _ -> False

-- -- similarly the Y combinator should fail typing
-- -- \lambda f . (\lambda x . f (x x)) (\lambda x . f (x x))
egYCombinator =
    blamVar (Text.pack "f") $ bapp fxx fxx
    where fxx = blamVar (Text.pack "x") $ bapp (bvt "f") (bapp (bvt "x") (bvt "x"))

testEgYCombinator = case fullTypeInfer M.empty egYCombinator of
    Right (Left (OccursFailure _ _)) -> True
    _ -> False

-- all the usual combinators of combinatory logic have a type
-- \f g x . f (g x)
egBCombinator = blamT "f" $ blamT "g" $ blamT "x" $ bapp (bvt "f") (bapp (bvt "g") (bvt "x"))
-- \f x y . f y x == \f x y . (f y ) x
egCCombinator = blamT "f" $ blamT "x" $ blamT "y" $ bapp (bapp (bvt "f") (bvt "y")) (bvt "x")
-- \x . x
egICombinator = blamT "x" (bvt "x")
-- \f x . f x x
egWCombinator = blamT "f" $ blamT "x" $ bapp (bapp (bvt "f") (bvt "x")) (bvt "x")
-- \x y . x
egKCombinator = blamT "x" $ blamT "y" (bvt "x")
-- \x y z . x z (y z)
egSCombinator = blamT "x" $ blamT "y" $ blamT "z" $ bapp (bapp (bvt "x") (bvt "z")) (bapp (bvt "y") (bvt "z"))

testCombinator comb = case fullTypeInferError M.empty comb of
    Right _ -> True
    _ -> False

testCombinators = map testCombinator [egBCombinator,egCCombinator,egICombinator,egKCombinator,egSCombinator,egWCombinator]

testAllCombinators = and testCombinators

egProdTerm1 :: BTerm Text
egProdTerm1 = bpair (bpair (bconst 1.1) (bconst 1.1)) (bconst 1.1) -- : ((R,R),R)
egProdTerm2 :: BTerm Text
egProdTerm2 = bpair (bconst 1.1) (bpair (bconst 1.1) (bconst 1.1)) -- : (R,(R,R))
egProdTerm10 = lProj egProdTerm1 -- (R,R)
egProdTerm11 = rProj egProdTerm1 -- R
egProdTerm100 = lProj egProdTerm10 -- R
egProdTerm101 = rProj egProdTerm10 -- R
egProdTerm20 = lProj egProdTerm2 -- R
egProdTerm21 = rProj egProdTerm2 -- (R,R)
egProdTerm210 = lProj egProdTerm21 -- R
egProdTerm211 = rProj egProdTerm21 -- R

testEgRRs = map testIsRR [egProdTerm10,egProdTerm21]
testEgRs = map testIsR [egProdTerm11,egProdTerm100,egProdTerm101,egProdTerm20,egProdTerm210,egProdTerm211]

testIsR t = case fullTypeInferError M.empty t of 
  Right (UTerm R) -> True
  _ -> False
testIsRR t = case fullTypeInferError M.empty t of
    Right (UTerm (TPair (UTerm R) (UTerm R))) -> True
    _ -> False



-- The tricky one 
projType :: Type
projType = UTerm (TArr (UTerm (TPair (UVar (IntVar 0)) (UVar (IntVar 1)))) (UVar (IntVar 0)))
badEnv = M.insert (Text.pack "f") projType M.empty
aTerm = bapp (bvt "f") $ bapp (bvt "f") $ bpair (bpair (bconst 1.1) (bconst 1.1)) (bconst 1.1)
checkATerm = fullTypeInferError (M.map fullyAbstractType badEnv) aTerm

testCheckATerm = case checkATerm of
    Right (UTerm R) -> True 
    _ -> False

typeChecks :: [Bool]
typeChecks = 
    testCombinators ++
    testEgRRs ++ 
    testEgRs ++
    [
        testEgTerm,
        test1EgTerm2,
        test1EgTerm3,
        test2EgTerm3,
        testEgTermMatchFailure,
        testegTermProjPair,
        testegTermProjPair2,
        testegTermProjPair3,
        testegTermProjPair4,
        testEgTermShadowBind,
        testEgTermDisregard,
        testEgTermDisregard2,
        testUnit,
        testEgTermUnitDisregard,
        testEgTermUnitDisregard2,
        testEgTermCodeEv,
        testEgTermCodeEv2,
        testEgLetBranchPair,
        testEgLetBranchPair2,
        testEgLetBranchPair3,
        testEgLetBranchPair4,
        testEgTermSelfApp,
        testEgYCombinator,
        testCheckATerm
    ]

allTestsPassed :: Bool
allTestsPassed = and typeChecks