module HasProj where

class HasRightProj a where 
    rProj :: a -> a

class HasLeftProj a where
    lProj :: a -> a