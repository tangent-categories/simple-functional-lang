module Language.AbsToBoundAbs where

import Language.Abs
import Language.BoundAbs
import TermUtils

import Bound

import Control.Applicative ((<|>))
import qualified Data.Set as S
import qualified Data.Text as Text
import Data.Text (Text)
import Language.Shape
import Paternalize (paternalize)
import qualified Data.Text.Read as Read


{-
The only failure condition is the failure of well formed patterns.
Technically it can fail on readMaybe when converting Data.Text to a double; however,
this will be caught by parsing.  Should convert from Maybe to giving the explicit 
location of the pattern error.
-}

lModToBoundModule :: LMod -> Maybe (BModule Text)
lModToBoundModule (LModule modName funDefs) = do 
    bFunDefs <- mapM lFunToBoundFunction funDefs
    return $ BModule bFunDefs

lFunToBoundFunction :: FunDef -> Maybe (BFunction Text)
lFunToBoundFunction (Function _ funName body) = do
    bBody <- absToBoundAbs body
    return $ BFunction (pIdentToText funName) bBody

getBFunNameBody :: BFunction a -> (a,BTerm a)
getBFunNameBody (BFunction a bt) = (a,bt)

absToBoundAbs :: Term -> Maybe (BTerm Text)
absToBoundAbs t = case t of
  App _ te te' -> do 
      m <- absToBoundAbs te 
      n <- absToBoundAbs te'
      return $ BApp m n
  Var _ x -> return $ BV (pIdentToText  x)
  Const _ d -> do 
      dub <- convertDoubleMaybe d
      return $ BConst dub
  Lam a pas m -> case pas of
    [] -> absToBoundAbs m
    (p : ps) -> do  -- \lambda p ps. m = \lambda p . \lambda ps . m
        psm <- absToBoundAbs (Lam a ps m)  -- \lambda ps . m
        (sh,absFun) <- patToShapeAbsFun p
        return $ BLam sh $ abstract absFun psm
  UnitTerm _ -> return BUnit 
  PairTerm _ m n -> do
      m' <- absToBoundAbs m
      n' <- absToBoundAbs n
      return $ BPair m' n'
  LetTerm _ p m n -> do
      (sh,absFun) <- patToShapeAbsFun p
      m' <- absToBoundAbs m
      let ms = paternalize sh m'
      n' <- absToBoundAbs n
      let n2 = abstract absFun n'
      return $ BLet ms n2
  FstTerm _ te -> do 
      m' <- absToBoundAbs te
      return $ BFst m'
  SndTerm _ te -> do 
      m' <- absToBoundAbs te
      return $ BSnd m'

convertDoubleMaybe :: Number -> Maybe Double
convertDoubleMaybe (Number (_, txt)) = case Read.rational txt of
  Left s -> Nothing
  Right (d,rest) -> Just d



--   | BLet [BTerm a] (Scope Int BTerm a)  so we have reduced the substitution to a simple

-- we could be fancier by peeking at m and remove a few fsts and snds, but just trying to keep it dead simple
-- paternalize :: Shape -> BTerm a -> [BTerm a]
-- paternalize sh m = case sh of
--   UShape -> []
--   Done -> [m]
--   PairS sh1 sh2 -> paternalize sh1 (BFst m) ++ paternalize sh2 (BSnd m)




-- Pattern utilities
{-
While converting a pattern to a shape and abs fun, we perform a side check 
for pattern well formedness.  The only condition for well-formedness is that 
the pattern doesn't repeat variables (i.e. patterns appear in the LHSs 
of left-linear rules)

The shape returned is the shape of the pattern.
The absFun :: Text -> Maybe Int is the function 
that allows creating a Scope Int TypeF Text using abstract.
  absFun a = Just n means a was the n-th variable from left to right in the pattern p.
E.g. if p = ((),(x,y))
Then absFun x = Just 0 and absFun y = Just 1
-}
patToShapeAbsFun :: Pat' a -> Maybe (Shape,Text -> Maybe Int)
patToShapeAbsFun p = do 
    (sh,absFun,_,_) <- patToShapeAbsFunAccum p S.empty (const Nothing) 0
    return (sh,absFun)

-- patToShapeAbsFunAccum :: _
-- What are partial signatures (putting _ in a type) in Haskell?  Does haskell support gradual typing now???
patToShapeAbsFunAccum ::  Pat' a -> S.Set Text -> (Text -> Maybe Int) -> Int -> Maybe (Shape, Text -> Maybe Int,S.Set Text,Int)
patToShapeAbsFunAccum p s f n = case p of 
  UnitPattern _ -> Just (UShape , const Nothing,s,n)
  Singleton _ x -> if S.member (pIdentToText x) s then Nothing else Just (Done,\a -> if pIdentToText x == a then Just n else f a,S.insert (pIdentToText  x) s,n+1)
  PairPattern _ p2 q2 -> do
      (sh,f2,s2,n2) <- patToShapeAbsFunAccum p2 s f n
      (sh2,f3,s3,n3) <- patToShapeAbsFunAccum q2 s2 f2 n2
      return (PairS sh sh2,\a -> f2 a <|> f3 a,s3,n3)

{-
This fails if the pattern is invalid -- i.e. if there is a duplicated variable.
-}
patToShape :: Pat' a -> Maybe Shape
patToShape p = do 
    (s,_) <- patToShapeAbsFun p
    return s
