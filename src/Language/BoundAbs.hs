{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE TemplateHaskell #-}

module Language.BoundAbs where

import Bound
import Control.Monad
import Data.Deriving (deriveEq1, deriveOrd1, deriveRead1, deriveShow1) 
import Data.Functor.Classes

import HasProj (HasLeftProj(..),HasRightProj(..))

import Language.Shape


-- should thread a module name through the parser and syntax of everything
newtype BModule a = BModule [BFunction a]

data BFunction a = BFunction a (BTerm a)

{-
HOAS 
Lam a (Term a)
Lam (Term a -> Term a)

Lam "x" (Var "x")   ~~~~>   Lam (\x -> Var x)

Scope Int Term a 
   The term with variables in A but where bound variables are abstracted with ints.

\(m)    ~~~> Scope () Term a
\ ((x,y),z) . m
\ Shape (m)
   m = App (Var "x") (Var "y")
\labmda (x,y) . m
  BLam (Done,Done)  (abstract (Data.List.elemIndex ["x","y"] (App (Var "x") (Var "y"))))
  Data.List.elemIndex "x" ["x","y"] = 0
-}

data BTerm a =
    BV a
    | BApp (BTerm a) (BTerm a)
    | BLam Shape (Scope Int BTerm a)
    | BConst Double
    | BUnit
    | BPair (BTerm a) (BTerm a)
    | BFst (BTerm a)
    | BSnd (BTerm a)
    | BLet [BTerm a] (Scope Int BTerm a)
    deriving (Functor,Foldable,Traversable)

instance HasLeftProj (BTerm a) where 
    lProj = BFst

instance HasRightProj (BTerm a) where
    rProj = BSnd

instance Applicative BTerm where 
    pure = BV
    (<*>) = ap

instance Monad BTerm where
    return = BV
    (BV a) >>= f = f a
    (BConst d) >>= _ = BConst d
    (BApp m n) >>= f = BApp (m >>= f) (n >>= f)
    (BLam sh m) >>= f = BLam sh (m >>>= f)
    BUnit >>= _ = BUnit
    (BPair m n) >>= f = BPair (m >>= f) (n >>= f)
    (BFst m) >>= f = BFst (m >>= f)
    (BSnd m) >>= f = BSnd (m >>= f)
    (BLet ms n) >>= f = BLet (map (>>= f) ms) (n >>>= f)



-- Boring classes

instance Eq a => Eq (BTerm a) where (==) = eq1
instance Ord a => Ord (BTerm a) where compare = compare1
instance Show a => Show (BTerm a) where showsPrec = showsPrec1
instance Read a => Read (BTerm a) where readsPrec = readsPrec1

-- Template haskell stuff to derive X1 where X is some derivable class
-- This is needed for higher-rank stuff.
deriveEq1   ''BTerm
deriveOrd1  ''BTerm
deriveRead1 ''BTerm
deriveShow1 ''BTerm