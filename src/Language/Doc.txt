The Language Language
BNF Converter


%Process by txt2tags to generate html or latex



This document was automatically generated by the //BNF-Converter//. It was generated together with the lexer, the parser, and the abstract syntax module, which guarantees that the document matches with the implementation of the language (provided no hand-hacking has taken place).

==The lexical structure of Language==

===Literals===



PIdent literals are recognized by the regular expression
`````('_' | letter) ('_' | digit | letter)*`````

Number literals are recognized by the regular expression
`````'-'? ('0' | digit+) ('.' digit+)? (["Ee"] '-'? digit+)?`````


===Reserved words and symbols===
The set of reserved words is the set of terminals appearing in the grammar. Those reserved words that consist of non-letter characters are called symbols, and they are treated in a different way from those that are similar to identifiers. The lexer follows rules familiar from languages like Haskell, C, and Java, including longest match and spacing conventions.

The reserved words used in Language are the following:
  | ``fst`` | ``in`` | ``let`` | ``snd``

The symbols used in Language are the following:
  | ; | = | ( | )
  | , | \ | . |

===Comments===
Single-line comments begin with //.Multiple-line comments are  enclosed with /* and */.

==The syntactic structure of Language==
Non-terminals are enclosed between < and >.
The symbols -> (production),  **|**  (union)
and **eps** (empty rule) belong to the BNF notation.
All other symbols are terminals.

  | //LMod// | -> | //[FunDef]//
  | //[FunDef]// | -> | **eps**
  |  |  **|**  | //FunDef//
  |  |  **|**  | //FunDef// ``;`` //[FunDef]//
  | //FunDef// | -> | //PIdent// //[Pat]// ``=`` //Term//
  | //[Pat]// | -> | **eps**
  |  |  **|**  | //Pat// //[Pat]//
  | //Pat// | -> | ``(`` ``)``
  |  |  **|**  | //PIdent//
  |  |  **|**  | ``(`` //Pat// ``)``
  |  |  **|**  | ``(`` //Pat// ``,`` //Pat// ``)``
  | //Term// | -> | //Term// //Term1//
  |  |  **|**  | //Term1//
  | //Term1// | -> | //PIdent//
  |  |  **|**  | //Number//
  |  |  **|**  | ``\`` //[Pat]// ``.`` //Term1//
  |  |  **|**  | ``(`` ``)``
  |  |  **|**  | ``(`` //Term1// ``,`` //Term1// ``)``
  |  |  **|**  | ``let`` //Pat// ``=`` //Term// ``in`` //Term1//
  |  |  **|**  | ``fst`` ``(`` //Term1// ``)``
  |  |  **|**  | ``snd`` ``(`` //Term1// ``)``
  |  |  **|**  | ``(`` //Term// ``)``



%% File generated by the BNF Converter (bnfc 2.9.3).
