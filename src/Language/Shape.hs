module Language.Shape where


{-
An unambiguous representation of an abstraction pattern shape.
For example, a nameless representation of (x,(y,())) is PairS Done (PairS Done UShape).
-}
data Shape = 
    UShape              -- Unit shape
    | Done              -- Variable shape/identity function shape
    | PairS Shape Shape --
    deriving (Eq,Ord,Read,Show)

