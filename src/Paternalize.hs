module Paternalize (paternalize) where

import Language.Shape
import HasProj

paternalize :: (HasLeftProj a,HasRightProj a) => Shape -> a -> [a]
paternalize s m = paternalizeAccum s m []

paternalizeAccum :: (HasLeftProj a,HasRightProj a) => Shape -> a -> [a] -> [a]
paternalizeAccum sh m acc = case sh of
  UShape -> acc
  Done -> m:acc
  PairS sh1 sh2 -> paternalizeAccum sh1 (lProj m) (paternalizeAccum sh2 (rProj m) acc) --right to left traversal of shape
  {-
  This is using a list as a stack so we push onto the stack during a right to left traversal of shape to accumulate 
  a list which, from left to right, is the list of shapes terms accumulated on a left to right traversal.

  That is:

  Define 
  paternalize' s m = case s of 
      UShape -> []
      Done -> [m]
      PairS sh1 sh2 -> paternalize' sh1 (lProj m) ++ paternalize' sh2 (rProj m)

  Then we are saying that for all sh,m 
      paternalize sh m = paternalize' sh m
  Lemma: paternalizeAccum sh m (acc ++ acc2) == (paternalizeAccum sh m acc) ++ acc2
  Proof.  By induction on sh
          When sh = UShape
            paternalizeAccum UShape m (acc ++ acc2) 
                = acc ++ acc2
                = (paternalizeAccum UShape m acc) ++ acc2
            When sh = Done 
              paternalizeAccum Done m (acc ++ acc2)
                = m:(acc ++ acc2) 
                = (m:acc) ++ acc2 -- by defn
                = (paternalizeAccum Done m acc) ++ acc2
            Finally, assuming the ind. hypotheses when sh = PairS sh1 sh2 we have 
              paternalizeAccum (PairS sh1 sh2) m (acc ++ acc2) 
                = paternalizeAccum sh1 (lProj m) (paternalizeAccum sh2 (rProj m) (acc ++ acc2))
                = paternalizeAccum sh1 (lProj m) ((paternalizeAccum sh2 (rProj m) acc) ++ acc2) -- ind hyp
                = (paternalizeAccum sh1 (lProj m) (paternalizeAccum sh2 (rProj m) acc)) ++ acc2 -- ind hyp
                = (paternalizeAccum (PairS sh1 sh2) m acc) ++ acc2
            Thus we have the identity required.
  Proposition: For all sh,m 
    paternalize sh m = paternalize' sh m
  Proof.  By induction on sh
          When sh = UShape
            paternalize UShape m 
                = paternalizeAccum UShape m [] 
                = []
                = paternalize' UShape m
          When sh = Done 
            paternalize Done m 
                = paternalizeAccum Done m []
                = [m]
                = paternalize' Done m
          Finally, assume the inductive hypotheses for sh = PairS sh1 sh2.
          Then 
            paternalize (PairS sh1 sh2) m 
                = paternalizeAccum (PairS sh1 sh2) m []
                = paternalizeAccum sh1 (lProj m) (paternalizeAccum sh2 (rProj m) [])
                = paternalizeAccum sh1 (lProj m) (paternalize' sh2 (rProj m))  -- Ind hyp
                = paternalizeAccum sh1 (lProj m) ([] ++ (paternalize' sh2 (rProj m)))
                = (paternalizeAccum sh1 (lProj m) []) ++ (paternalize' sh2 (rProj m)) -- Lemma
                = paternalize' sh1 (lProj m) ++ paternalize' sh2 (rProj m)  -- Ind hyp
                = paternalize' (PairS sh1 sh2) m
          Thus we have the identity required.        
  -}

