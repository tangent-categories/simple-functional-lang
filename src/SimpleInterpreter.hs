module SimpleInterpreter where

import qualified Data.Map as M
import Data.Text (Text)
import qualified Data.Text as Text
import Control.Monad.State.Strict
import Language.Abs
import TermUtils
import Control.Monad.Except (MonadError(throwError))



type ValueEnv a = M.Map Text (Term' a)
type MResult a b = StateT (ValueEnv a) (Either String) b

-- return the term and state whether a step happened or not.
-- It would be more efficient to actually return the location of the subterm we were at, rather than working down from the top node each time...
-- To 

-- We reduce to weak head normal form.  This means that we do not reduce under lambda (weak) and if the top level node is an application, 
-- where the function position is not an abstraction, we're notDone.
{-
To state this simply:
   1. WHNF(x) for any atom x (variable or literal)
   2. For any m, WHNF(\lambda x.m)
   3. For any m if WHNF m and m \ne \lambda x .m' then for any n WHNF(m n)

If step returns (m,False), then there are no redexes in the term, nor can there be a redex 
triggered by substitution.  In this case we substitute out the term.  Note that we are *not*
at this time performing a lazy or shared substitution (simply due to variable renamingn)

But we still have to solve the bound variable issue ... which is as always, annoying...
-}
step :: Term' a -> MResult a (Term' a,Bool)
step t = case t of
  -- an abstraction (\lambda .m) n with no variables extracted is k m n = m.  In this case a reduction did occur.
  App _ (Lam _ [] m) n -> return (m,True)
  -- an abstraction (\lambda x xs.m) n = \lambda xs.(m[n/x])
  -- in this case a reduction did occur
  App a (Lam _ (x:xs) m) n -> do
      return  (LetTerm a x n m,True)
  -- The final case is when the application is not a redex.  In this case, as we are only doing head reductions, we only search for one in the head redex
  App a m n -> do
      (m',notDone) <- step m
      return (App a m' n,notDone)
  Const a x -> return (Const a x,False)
  -- we are going to head normal form so no peeking under lambdas
  Lam a pas m -> return (Lam a pas m,False)
  UnitTerm a -> return (UnitTerm  a,False)
  PairTerm a m n -> do
      (m',notDone) <- step m
      if notDone
          then return (PairTerm a m' n,True)
          else do
              (n',notDoneNow) <- step n
              return (PairTerm a m n',notDoneNow)
  FstTerm _ (PairTerm _ m n) -> return (m,True)
  FstTerm a m -> do
      (m',notDone) <- step m
      return (FstTerm a m',notDone)
  SndTerm _ (PairTerm _ m n) -> return (n,True)
  SndTerm a m -> do
      (m',notDone) <- step m
      return (SndTerm a m',notDone)
  LetTerm a pa m n -> case pa of
    -- let () = m in n == n
    (UnitPattern _) -> return (n,True)
    (Singleton a' pi) -> do
        env <- get
        modify (M.insert (pIdentToText pi) m)
        n' <- subOut n
        put env
        return n'
    -- here we are subbing m for (p,q) in n.  If m is already of the form of a pair, then we create two substitutions
    -- and move on.  Otherwise, we make m into a pair by m = (fst(m),snd(m))
    (PairPattern a' p q) -> case m of
        PairTerm _ ml mr -> return (LetTerm a' p ml $ LetTerm a' q mr n,True) -- m = (ml,mr) so n[m/(p,q)] = n[(ml,mr)/(p,q)] = n[ml/p][mr/q]
        _ -> return (LetTerm a' p (FstTerm a' m) $ LetTerm a' q (SndTerm a' m) n,True)
  Var a pi -> subOut (Var a pi)

subOut :: Term' a -> MResult a (Term' a,Bool)
subOut t = case t of
  App a m n -> do
      (m',notDoneM) <- subOut m
      (n',notDoneN) <- subOut n
      return (App a m' n',notDoneM || notDoneN)
  Var a pi -> do
      valM <- gets $ M.lookup (pIdentToText pi)
      case valM of
        Nothing -> return (Var a pi,False) -- lift $ throwError $ "Variable not in scope: " ++ pIdentToErr pi
        Just te -> return (te,True)
  Const a x -> return (Const a x,False)
  -- I think this algorithm, as constructed, will perform variable capture...
  Lam a pas m -> do
      env <- get
      let binds = patVarsList pas
      modify $ \env -> M.difference env binds -- remove the bound variables from the environment
      m' <- subOut m
      put env -- reset the environment
      return m'

  UnitTerm a -> return (UnitTerm a,False)
  PairTerm a m n -> do
      (m',notDoneM) <- subOut m
      (n',notDoneN) <- subOut n
      return (PairTerm a m' n',notDoneM || notDoneN)
  -- we might as well sub-out terms we meet along the way
  -- let n[m/()] = n
  LetTerm a (UnitPattern b) m n -> subOut n
  LetTerm a (Singleton b x) m n -> do
    -- subout m
    (m',_) <- subOut m
    -- add x=m to the environment, overwriting any x=v currently in the environment
    -- the reason we overwrite the environment is that the variable x is now more local
    -- and we use non-capturing substitution, so we use the more recent binding
    env <- get
    modify $ M.insert (pIdentToText x) m'
    (n',_) <- subOut n
    put env -- restore the environment
    return (n',True)
  LetTerm a (PairPattern b p q) (PairTerm c ml mr) n -> do
      (sbbd,_) <- subOut $ LetTerm a p ml $ LetTerm a q mr n -- just being easy on ourselves...
      return (sbbd,True)
  LetTerm a (PairPattern b p q) m n -> do
      (sbbd,_) <- subOut $ LetTerm a p (FstTerm a m) $ LetTerm a q (SndTerm a m) n
      return (sbbd,True)
  FstTerm a m -> do
      (m',notDone) <- subOut m
      return (FstTerm a m',notDone)
  SndTerm a m -> do
      (m',notDone) <- subOut m
      return (SndTerm a m',notDone)

fullEval :: Term' a -> MResult a (Term' a)
fullEval m = do
    (m',notDone) <- step m
    if notDone then fullEval m' else return m'

fullEvalWithEnv :: Term' a -> ValueEnv a -> Either String (Term' a)
fullEvalWithEnv m = evalStateT (fullEval m)

{-
Here's where some meta-theorems can be very useful.
Like if m :: (a,b) then m ~~> (m1,m2) or m is a variable.
-}


-- Debugging crap

xypat = pPat (sPat "x") (sPat "y")
first = lamPat xypat (var "x")
firstTerm = app first (pairT (pairT (constT 1.1) (constT 1.1)) (constT 1.1))
firstFirstTerm = app first firstTerm
valEnv = M.insert (Text.pack "first") first M.empty
