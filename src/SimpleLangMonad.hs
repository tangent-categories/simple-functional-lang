module SimpleLangMonad where

import Language.ErrM
import Control.Monad.Except
import Control.Monad.Identity

errMToException :: (Monad m) => Err a -> ExceptT String m a
errMToException err = case err of 
  Left s -> throwError s
  Right a -> return a

-- errToLog :: (Monad m) => Either String a -> 

type SimpleLangMonadT e m a = ExceptT e m a
type SimpleLangMonad a = ExceptT String Identity a


runSLangT :: (Monad m) => SimpleLangMonadT e m a -> m (Either e a)
runSLangT = runExceptT 

runSLang :: SimpleLangMonad a -> Either String a
runSLang = runIdentity . runSLangT

liftBoundMaybe :: (Monad m) => String -> Maybe a -> ExceptT String m a
liftBoundMaybe item m = case m of 
  Nothing -> throwError $ "Ill defined pattern at:  " ++ item
  Just a -> return a
