module TermUtils where

import Language.Abs
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Map as M

import HasProj (HasLeftProj(..),HasRightProj(..))

class Ignorable a where 
    ignorantPoint :: a

instance Ignorable (Maybe a) where 
    ignorantPoint = Nothing

instance Ignorable () where 
    ignorantPoint = ()

-- Since Abs.hs is created from codegen, we give additional instances here
instance Ignorable a => HasLeftProj (Term' a) where
    lProj = FstTerm ignorantPoint

instance Ignorable a => HasRightProj (Term' a) where 
    rProj = SndTerm ignorantPoint

-- In particular the Terms created by by term utils are Term' () and the Terms created by parsing are Term' (Maybe (Int,Int))
-- Honestly the functor instance on for BNFC could be improved.
-- data Expr a = Var a | ... | Node a [Expr a]
-- Then for positional parsing you take type PosnExpr a =  Expr (Maybe a,Position)  or even type PosnExpr2 a = Expr Maybe (Maybe a,Position)
-- Then in PosnExpr we have Var (Just "x",(row,col))  and on nodes we could Node (Nothing,(row,col)) subTrees  
-- In PosnExpr2 we have Var (Just (Just "x",(row,col))) and on nodes we could have Node (Just (Nothing,(row,col))) subTrees  or even Node Nothing subtrees.
-- But the point is that we can now have more canonical term operations, rather than doing Var _ x -> ... all the time.

-- move this to a syntax util file

getNameBody :: FunDef' a -> (PIdent,Term' a)
getNameBody (Function _ name body) = (name,body)

type Set a = M.Map a ()


pIdentToText :: PIdent -> Text
pIdentToText (PIdent ((_,_),name)) = name

-- a = Function a PIdent (Term' a)

freeVarsFunction :: FunDef' a -> Set Text
freeVarsFunction (Function _ _ body) = freeVars body

freeVars :: Term' a -> Set Text
freeVars t = freeVarsAccum t M.empty

{-
avoid creating singleton at all leaves and avoid expensive union
somewhat offset as insetion is logn

Invariant: freeVarsAccum t s == freeVars t \cup s
-}
freeVarsAccum :: Term' a -> Set Text -> Set Text
freeVarsAccum t accum = case t of
    Var _ x -> M.insert (pIdentToText x) () accum
    Const _ d -> accum
    UnitTerm _ -> accum
    App _ m n -> freeVarsAccum m (freeVarsAccum n accum)
    PairTerm _ m n -> freeVarsAccum m (freeVarsAccum n accum)
    -- fv(let x = m in n) = fv(m) \cup (fv(n)\{x})
    LetTerm _ p m n -> freeVarsAccum m (M.difference (freeVarsAccum n accum) (patVars p))
    Lam _ ps m -> M.difference (freeVarsAccum m accum) (patVarsList ps)
    FstTerm _ m -> freeVarsAccum m accum
    SndTerm _ m -> freeVarsAccum m accum

patVars :: Pat' a -> Set Text
patVars p = patVarsAccum p M.empty

patVarsList :: [Pat' a] -> Set Text
patVarsList = foldr patVarsAccum M.empty
-- patVarsList [] = M.empty 
-- patVarsList (p:ps) = patVarsAccum p (patVarsList ps)

listPatVars :: Pat' a -> [Text]
listPatVars p = listPatVarsAccum p []

listPatVarsAccum :: Pat' a -> [Text] -> [Text]
listPatVarsAccum p acc = case p of
  UnitPattern _ -> []
  Singleton _ x -> pIdentToText x : acc
  PairPattern _ p q -> listPatVarsAccum p (listPatVarsAccum q acc)



{-
Invariant: patVarsAccum p s == patVars s \cup s
-}
patVarsAccum :: Pat' a -> Set Text -> Set Text
patVarsAccum p accum = case p of
    UnitPattern _ -> accum
    Singleton _ x -> M.insert (pIdentToText x) () accum
    PairPattern _ m n -> patVarsAccum m (patVarsAccum n accum)

{-
Need to test for pattern well-formedness.  Probably can do as part of type checking.
A pattern cannot repeat variables.
-}

pIdentToErr :: PIdent -> String
pIdentToErr (PIdent ((r,c),x)) = Text.unpack x ++ " at: " ++ " Line: " ++ show r ++ " Column: " ++ show c


-- term builder helpers
var :: String -> Term' ()
var x = Var () (s2p x)
constT :: Double -> Term' ()
constT d = Const () (Number ((0,0),Text.pack (show d)))
app :: Term' () -> Term' () -> Term' ()
app = App ()
unitT :: Term' ()
unitT = UnitTerm ()
pairT :: Term' () -> Term' () -> Term' ()
pairT = PairTerm ()
letVar :: String -> Term' () -> Term' () -> Term' ()
letVar x = LetTerm () (sPat x)
letPat :: Pat' () -> Term' () -> Term' () -> Term' ()
letPat = LetTerm ()
lamVar :: String -> Term' () -> Term' ()
lamVar x = Lam () [sPat x]
lamPat :: Pat' () -> Term' () -> Term' ()
lamPat p = Lam () [p]
lamPats :: [Pat' ()] -> Term' () -> Term' ()
lamPats = Lam ()
fstTerm :: Term' () -> Term' ()
fstTerm = FstTerm ()
sndTerm :: Term' () -> Term' ()
sndTerm = SndTerm ()

-- pattern builder helpers
uPat :: Pat' ()
uPat = UnitPattern ()
sPat :: String -> Pat' ()
sPat = stringToPatternDefault
pPat :: Pat' () -> Pat' () -> Pat' ()
pPat = PairPattern ()


stringToPIdentDefault :: String -> PIdent
stringToPIdentDefault x = PIdent  ((0,0),Text.pack x)
s2p :: String -> PIdent
s2p = stringToPIdentDefault
stringToPatternDefault :: String -> Pat' ()
stringToPatternDefault = Singleton () . stringToPIdentDefault